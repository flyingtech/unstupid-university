<!DOCTYPE HTML>
<html>
    <head>
        <title>
            About - Unstupid University
        </title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css">
        <link href="css/wodry.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="css/helpers/jquery.fancybox-media.js" type="text/javascript"></script>
        <script src="js/wodry.js" type="text/javascript"></script>
        <script src="js/html5shiv.js" type="text/javascript"></script>
        <script src="js/modernizr.custom.51810.js" type="text/javascript"></script>
        <script src="js/respond.js" type="text/javascript"></script>
        <script src="js/init.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="about-wrapper">
                        <!-- Header Starts Here -->
                        <div class="header">
                            <div class="header-logo">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="header-bg">
                                            <div class="unstupid-logo col-sm-5 col-xs-12">
                                                <a href="#"> <img src="images/unstupid-logo.png" class="img-responsive"/> </a>
                                            </div>
                                            <div class="unstupid-logo-text col-sm-7 col-xs-12">
                                                <span class="wodry">The finest institute for the hopelessly stupid</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-menu-bar">
                                <div class="top-nav">
                                    <ul>
                                        <li>
                                            <a href="#">  <img src="images/home.gif" height="35" width="35"> </a>
                                            <a href="#" class="btn btn-info menu-item" role="button">Home</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-info menu-item" role="button">Courses</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-info menu-item" role="button">Exam of the Day</a>
                                        </li>
                                        <li class="last-item">
                                            <a href="#" class="btn btn-info menu-item" role="button">About the Show</a>
                                        </li>
                                        <li>
                                        </li>
                                    </ul>
                                    <div class="share-icons-home">
                                        <!-- AddToAny BEGIN -->
                                        <!-- https://www.addtoany.com/buttons/for/website -->
                                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                            <a class="a2a_dd" href="https://www.addtoany.com/share_save?linkurl=http%3A%2F%2Fnatgeounstupiduniversity.com%2F&amp;linkname=Unstupid%20University"></a>
                                            <a class="a2a_button_facebook"></a>
                                            <a class="a2a_button_twitter"></a>
                                            <a class="a2a_button_google_plus"></a>
                                        </div>
                                        <script type="text/javascript">
                                            var a2a_config = a2a_config || {};
                                            a2a_config.linkname = "Nat Geo - Unstupid University";
                                            a2a_config.linkurl = "http://natgeounstupiduniversity.com/";
                                        </script>
                                        <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                                        <!-- AddToAny END -->
                                        <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                                        <!-- AddToAny END -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Header Ends -->

                        <div class="about-body-wrapper">
                            <div class="about-copy">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis  nostrud exercitation ullamco laboris nisi ut aliquip 
                                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in premieres 15 August, 9 PM
                                </p>
                            </div>
                            <div class="video-iframe">
                                <iframe width="750" height="425" src="https://www.youtube.com/embed/V0v_WH6pvzI" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="home-add-banner">   <!-- Add banner starts Here --> 
                            <img src="images/desktop-banner.png" class="img-responsive desktop-banner">
                            <img src="images/mobile-banner.jpg" class="img-responsive mobile-banner">
                        </div> <!-- Add banner Ends Here --> 

                        <div class="footer"> <!-- Footer starts Here --> 
                            <div class="terms-footer">
                                <p>
                                    &copy; Unstupid University, 2015  |   <a href="#">Terms and Conditions</a>
                                </p>
                            </div>
                            <div class="logo-footer">
                                <div class="ngc-logo">
                                    <a href="#">  <img src="images/ngc-logo.png"> </a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div> <!-- Footer Ends Here -->
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>