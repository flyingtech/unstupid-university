<!DOCTYPE HTML>
<html>
    <head>
        <title>
            Home - Unstupid University
        </title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css">
        <link href="css/wodry.css" rel="stylesheet" type="text/css"/>
        <link href="style.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>
        <script src="css/helpers/jquery.fancybox-media.js" type="text/javascript"></script>
        <script src="js/wodry.js" type="text/javascript"></script>
        <!-- AddThisEvent -->
        <script type="text/javascript" src="https://addthisevent.com/libs/1.6.0/ate.min.js"></script>
        <script src="js/html5shiv.js" type="text/javascript"></script>
        <script src="js/modernizr.custom.51810.js" type="text/javascript"></script>
        <script src="js/respond.js" type="text/javascript"></script>
        <script src="js/init.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="home-wrapper">
                        <!-- Header Starts Here -->
                        <div class="header">
                            <div class="header-logo">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="header-bg">
                                            <div class="unstupid-logo col-sm-5 col-xs-12">
                                                <a href="index.php"> <img src="images/unstupid-logo.png" class="img-responsive"/> </a>
                                            </div>
                                            <div class="unstupid-logo-text col-sm-7 col-xs-12">
                                                <!--<span class="wodry">The finest institute for the hopelessly stupid</span>-->
                                                <figure>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                    <h1>The finest institute for the hopelessly stupid</h1>
                                                </figure>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-menu-bar">
                                <div class="top-nav">
                                    <ul class="menu-tabs">
                                        <li>
                                            <a href="index.php">  <img src="images/home.gif" height="35" width="35"> </a>
                                            <a href="index.php" class="btn btn-info menu-item" role="button">Home</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-info menu-item" role="button">Courses</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-info menu-item" role="button">Exam of the Day</a>
                                        </li>
                                        <li class="last-item">
                                            <a href="about.php" class="btn btn-info menu-item" role="button">About the Show</a>
                                        </li>
                                    </ul>
                                    <div class="share-icons-home">
                                        <!-- AddToAny BEGIN -->
                                        <!-- https://www.addtoany.com/buttons/for/website -->
                                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                            <a class="a2a_dd" href="https://www.addtoany.com/share_save?linkurl=http%3A%2F%2Fnatgeounstupiduniversity.com%2F&amp;linkname=Unstupid%20University"></a>
                                            <a class="a2a_button_facebook"></a>
                                            <a class="a2a_button_twitter"></a>
                                            <a class="a2a_button_google_plus"></a>
                                        </div>
                                        <script type="text/javascript">
                                            var a2a_config = a2a_config || {};
                                            a2a_config.linkname = "Nat Geo - Unstupid University";
                                            a2a_config.linkurl = "http://natgeounstupiduniversity.com/";
                                        </script>
                                        <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                                        <!-- AddToAny END -->
                                        <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
                                        <!-- AddToAny END -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Header Ends -->
                        <div class="home-body-wrap">
                            <div class="video-container"> <!-- Video container Starts Here -->
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 video-wrap">
                                        <a href="https://www.youtube.com/embed/V0v_WH6pvzI?autoplay=1&rel=0&autohide=1" class="fancybox fancybox.iframe">
                                            <img src="images/video-bg.png" class="img-responsive">
                                        </a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 rules-wrap">

                                        <div class="mission-block">
                                            <marquee direction="up" scrollamount="1" loop="repeat">
                                                <div>
                                                    <h2 class="green">What is UnStupid University?</h2>
                                                    <p>
                                                        Everyday stupidity is on the rise. Hum isse change karna chahte hai. Every stupid needs to be educated. Is tarah, UnStupid University was born.
                                                    </p>
                                                    <h2 class="green">Mission:</h2>
                                                    <p>
                                                        To teach every stupid ki tumhe jo bhi sahi lagta hai, woh galat hai.
                                                    </p>
                                                    <h2 class="green">Motto:</h2>
                                                    <p>
                                                        Duniya ko badal de, stupidity ko mita ke.
                                                    </p>
                                                    <h2 class="green">Objective:</h2>
                                                    <p>
                                                        <i class="fa fa-circle fa-1x"></i> Eliminate stupidity
                                                        <br/>
                                                        <i class="fa fa-circle fa-1x"></i> Eliminate stupidity
                                                        <br/>
                                                        <i class="fa fa-circle fa-1x"></i> Eliminate stupidity
                                                    </p>
                                                </div>
                                            </marquee>
                                        </div>

                                        <div class="call-block">
                                            <img src="images/CallManish.gif" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 bottom-blue-patch">
                                    <div class="col-sm-5 col-xs-4 exam-copy">
                                        <div class="row">
                                            <marquee> <div class="col-sm-4 col-xs-12 remove-padding">Exam of the day!!! </div>
                                                <div class="col-sm-4 col-xs-12 remove-padding hide-in-phone">Exam of the day!!! </div>
                                                <div class="col-sm-4 col-xs-12 remove-padding hide-in-phone">Exam of the day!!! </div> </marquee>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-4 remove-padding">
                                        <div class="test-btn">
                                            <a href="#"><img src="images/take-test-button.png" class="img-responsive"></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 col-xs-4 exam-copy">
                                        <div class="row">
                                            <marquee>
                                                <div class="col-sm-4 col-xs-12 remove-padding">Exam of the day!!! </div>
                                                <div class="col-sm-4 col-xs-12 remove-padding hide-in-phone">Exam of the day!!! </div>
                                                <div class="col-sm-4 col-xs-12 remove-padding hide-in-phone">Exam of the day!!! </div>
                                            </marquee>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- Video container Ends Here -->
                            <div class="clearfix"></div>
                            <div class="home-add-banner">   <!-- Add banner starts Here --> 
                                <a href="https://www.youtube.com/watch?v=V0v_WH6pvzI" target="_blank"> <img src="images/desktop-banner.png" class="img-responsive desktop-banner"> </a>
                                <a href="https://www.youtube.com/watch?v=V0v_WH6pvzI" target="_blank"><img src="images/mobile-banner.jpg" class="img-responsive mobile-banner"></a>
                            </div> <!-- Add banner Ends Here --> 

                            <!-- Event Button Starts  Here --> 
                            <div title="Add to Calendar" class="addthisevent event-btn" style="background: #ec7f1b; color: #fff !important;">
                                Add to Calendar
                                <span class="start">08/15/2015 12:00 PM</span>
                                <span class="end">08/15/2015 9:00 PM</span>
                                <span class="timezone">Asia/Kolkata</span>
                                <span class="title">Science Of Stupid New Season</span>
                                <span class="description">Get ready to join Manish Paul as he teaches you the science behind all those stupid stunts you pull. Tonight, at 9 PM. Only on Nat Geo.</span>
                                <span class="organizer">National Geographic Channel</span>
                                <span class="facebook_event">https://www.facebook.com/events/1462977720692925/</span>
                                <span class="all_day_event">false</span>
                                <span class="date_format">MM/DD/YYYY</span>
                            </div>
                            <!-- Event Button Ends Here --> 
                            <div class="clearfix"></div>
                            <div class="home-courses">
                                <div class="row">
                                    <div class="col-sm-2 col-xs-6 first-course-block">
                                        <img src="images/courser-start.png" class="img-responsive"/>
                                    </div>
                                    <div class="col-sm-2 col-xs-6">
                                        <div class="courses-wrap">
                                            <div class="courses-block">
                                                <div class="overlay-copy">
                                                    <article>
                                                        For your<br/>
                                                        'Dekh sakta, lekin phir bhi andha hai' friend
                                                    </article>
                                                    <a href="#">VIEW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-6">
                                        <div class="courses-wrap">
                                            <div class="courses-block">
                                                <div class="overlay-copy">
                                                    <article>
                                                        For your<br/>
                                                        'Are you for real?' dost
                                                    </article>
                                                    <a href="#">VIEW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-6">
                                        <div class="courses-wrap">
                                            <div class="courses-block">
                                                <div class="overlay-copy">
                                                    <article>
                                                        For your<br/>
                                                        'fail khiladi' friend
                                                    </article>
                                                    <a href="#">VIEW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-6">
                                        <div class="courses-wrap">
                                            <div class="courses-block">
                                                <div class="overlay-copy">
                                                    <article>
                                                        For your<br/> 'technologically challenged' dost
                                                    </article>
                                                    <a href="#">VIEW</a>
                                                    <p>
                                                        <img src="images/new_red.gif"/>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-xs-6">
                                        <div class="courses-wrap">
                                            <div class="courses-block">
                                                <div class="overlay-copy">
                                                    <article>
                                                        For your<br/>
                                                        'Tries too hard'
                                                        dost
                                                    </article>
                                                    <a href="#">VIEW</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="home-notice">
                                <p>
                                <div class="blinker">  <strong>Notice:</strong> Every student must wear helmets in class.    |     Shaili Ranjan successfully managed to not walk into a glass door.    |    Entries for the placement cell are open.</div>  

                                </p>
                            </div>
                            <div class="footer"> <!-- Footer starts Here --> 
                                <div class="terms-footer">
                                    <p>
                                        &copy; Unstupid University, 2015  |   <a href="#">Terms and Conditions</a>
                                    </p>
                                </div>
                                <div class="logo-footer">
                                    <div class="ngc-logo">
                                        <a href="#">  <img src="images/ngc-logo.png"> </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div> <!-- Footer Ends Here --> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>