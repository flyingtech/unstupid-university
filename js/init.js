$(document).ready(function () {

    /* Menu active class */
    $(".menu-tabs li a").on("click", function () {
        $(".menu-tabs li").find(".active").removeClass("active");
        $(this).parent().addClass("active");
    });


    /* Header Text rotation */

    $('.wodry').wodry({
        animation: 'rotateY',
        delay: 1000,
        animationDuration: 300
    });

    /* Home Page Video Fancy Box */
    $(".fancybox").fancybox();
});